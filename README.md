Autocondat AdminBundle
======================

Autocondat Admin Bundle is the Core of Autocondat Project. Is required by the rest of the modules
and integrates naviation, alerts, FOSUserBundle, Permissions, Roles, Calendar, etc.

Features include:

- Users can be stored via Doctrine ORM or MongoDB/CouchDB ODM
- Registration support, with an optional confirmation per email
- Password reset support
- Unit tested

**Note:** This bundle does *not* provide an authentication system but can
provide the user provider for the core [SecurityBundle](https://symfony.com/doc/current/book/security.html).

[![Build Status](https://travis-ci.org/autocondat/AdminBundle.svg?branch=master)](https://travis-ci.org/autocondat/AdminBundle)
[![Latest Stable Version](https://poser.pugx.org/autocondat/admin-bundle/v/stable)](https://packagist.org/packages/autocondat/admin-bundle)
[![Total Downloads](https://poser.pugx.org/autocondat/admin-bundle/downloads)](https://packagist.org/packages/autocondat/admin-bundle)
[![Latest Unstable Version](https://poser.pugx.org/autocondat/admin-bundle/v/unstable)](https://packagist.org/packages/autocondat/admin-bundle)
[![Stories in Ready](https://badge.waffle.io/autocondat/AdminBundle.svg?label=ready&title=Ready)](http://waffle.io/autocondat/AdminBundle)
[![License](https://poser.pugx.org/autocondat/admin-bundle/license)](https://packagist.org/packages/autocondat/admin-bundle)

Documentation
-------------

The source of the documentation is stored in the `Resources/doc/` folder
in this bundle, and available on symfony.com:

[Read the Documentation for master](https://symfony.com/doc/master/bundles/FOSUserBundle/index.html)

[Read the Documentation for 1.3.x](https://symfony.com/doc/1.3.x/bundles/FOSUserBundle/index.html)

Installation
------------

All the installation instructions are located in the documentation.

License
-------

This bundle is under the MIT license. See the complete license [in the bundle](LICENSE)

About
-----

UserBundle is a [knplabs](https://github.com/knplabs) initiative.
See also the list of [contributors](https://github.com/FriendsOfSymfony/FOSUserBundle/contributors).

Reporting an issue or a feature request
---------------------------------------

Issues and feature requests are tracked in the [Github issue tracker](https://github.com/FriendsOfSymfony/FOSUserBundle/issues).

When reporting a bug, it may be a good idea to reproduce it in a basic project
built using the [Symfony Standard Edition](https://github.com/symfony/symfony-standard)
to allow developers of the bundle to reproduce the issue by simply cloning it
and following some steps.
